build:
	- go build -o {appName}

run:
	- go run main.go run ./.env

migrate:
	- go run main.go migrate ./.env