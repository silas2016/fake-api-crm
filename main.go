package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/silas2016/fake-api-crm/api/config"
)

func main() {
	argsCommandLine := os.Args[1:]
	if len(argsCommandLine) > 1 {
		argReceived := argsCommandLine[0]
		argEnvPath := argsCommandLine[1]
		err := godotenv.Load(argEnvPath)
		if err != nil {
			log.Fatal("Erro loading .env file")
		}

		if argReceived == "run" {
			instance := config.Instance{}
			instance.Run()
		} else {
			fmt.Println("Command not found")
		}
	} else {
		fmt.Println("(run = run server | migrate = run migrations database) /path/.env")
	}
}
