package database

import (
	"fmt"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func CreateConnection() *gorm.DB {
	var err error
	DB_HOST := os.Getenv("DB_HOST")
	DB_DATABASE := os.Getenv("DB_DATABASE")
	DB_USERNAME := os.Getenv("DB_USERNAME")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")
	DB_PORT := os.Getenv("DB_PORT")
	DB_URL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DB_HOST, DB_PORT, DB_USERNAME, DB_DATABASE, DB_PASSWORD)
	db, err := gorm.Open(postgres.Open(DB_URL), &gorm.Config{})
	if err != nil {
		fmt.Printf("Erro ao conectar no banco de dados: %v\n", err)
		panic("Erro ao conectar a base de dados.")
	}

	sqlDB, err := db.DB()
	if err != nil {
		fmt.Print(err)
		panic("Error ao configurar opções da conexão com o postgres")
	}

	// quantidade de conexoes na poll
	sqlDB.SetMaxIdleConns(0)

	sqlDB.SetMaxOpenConns(5)
	sqlDB.SetConnMaxLifetime(3 * time.Minute)

	return db
}
