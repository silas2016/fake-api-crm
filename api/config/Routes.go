package config

import "gitlab.com/silas2016/fake-api-crm/api/routes"

func (instance *Instance) LoadRoutes() {
	routes.CustomerRoutes(instance.Server, instance.Database)
}
