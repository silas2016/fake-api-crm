package config

import (
	"fmt"
	"os"

	"gitlab.com/silas2016/fake-api-crm/api/config/database"
	"gorm.io/gorm"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

type Instance struct {
	Server   *echo.Echo
	Database *gorm.DB
}

func (instance *Instance) Run() {

	err := godotenv.Load()
	if err != nil {
		fmt.Println("Erro ao carregar conteúdo do .env")
	}

	instance.Server = echo.New()

	instance.Database = database.CreateConnection()

	instance.LoadRoutes()

	port := fmt.Sprintf(":%s", os.Getenv("APP_PORT"))

	if port == "" {
		port = fmt.Sprintf(":%d", 7000)
	}

	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	instance.Server.Start(port)
}
