package routes

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/silas2016/fake-api-crm/api/controllers"
	"gorm.io/gorm"
)

func CustomerRoutes(e *echo.Echo, db *gorm.DB) {
	customerCtrl := controllers.MakeCustomerController(db)

	e.POST("/api/v1/customers", customerCtrl.Create)
}
