package repositories

import (
	"gitlab.com/silas2016/fake-core-crm/core/entities"
	"gorm.io/gorm"
)

type EmailRepo struct {
	DB *gorm.DB
}

type EmailModel struct {
	ID         int    `gorm:"column:id;primary key"`
	Email      string `gorm:"column:email"`
	CustomerID int    `gorm:"column:customer_id"`
}

func (em *EmailModel) TableName() string {
	return "emails"
}

func (em *EmailModel) ToEmailEntity() entities.Email {
	return entities.Email{
		ID:         em.ID,
		Email:      em.Email,
		CustomerID: em.CustomerID,
	}
}

func ToListEmailEntity(emailsMdl []EmailModel) []entities.Email {
	emails := make([]entities.Email, 0)

	for i := 0; i < len(emailsMdl); i++ {
		emails = append(emails, emailsMdl[i].ToEmailEntity())
	}

	return emails
}

func FromEmailEntityToEmailModel(email entities.Email, customerID int) EmailModel {
	return EmailModel{
		Email:      email.Email,
		CustomerID: customerID,
	}
}

func FromListEmailEntityToListEmailModel(emails []entities.Email, customerID int) []EmailModel {
	emailsMdl := make([]EmailModel, 0)

	for i := 0; i < len(emails); i++ {
		emailsMdl = append(emailsMdl, FromEmailEntityToEmailModel(emails[i], customerID))
	}

	return emailsMdl
}

func (er *EmailRepo) SaveBatch(emails []entities.Email, customerID int) ([]entities.Email, error) {
	emailsMdl := FromListEmailEntityToListEmailModel(emails, customerID)

	err := er.DB.Model(&EmailModel{}).CreateInBatches(&emailsMdl, 10).Error
	if err != nil {
		return emails, err
	}

	emails = ToListEmailEntity(emailsMdl)

	return emails, nil
}
