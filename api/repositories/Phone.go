package repositories

import (
	"gitlab.com/silas2016/fake-core-crm/core/entities"
	"gorm.io/gorm"
)

type PhoneRepo struct {
	DB *gorm.DB
}

type PhoneModel struct {
	ID         int    `gorm:"column:id;primary key"`
	Number     string `gorm:"column:number"`
	CustomerID int    `gorm:"column:customer_id"`
}

func (pm *PhoneModel) TableName() string {
	return "phones"
}

func (pm *PhoneModel) ToPhoneEntity() entities.Phone {
	return entities.Phone{
		ID:         pm.ID,
		Number:     pm.Number,
		CustomerID: pm.CustomerID,
	}
}

func ToListPhoneEntity(phonesMdl []PhoneModel) []entities.Phone {
	phones := make([]entities.Phone, 0)

	for i := 0; i < len(phonesMdl); i++ {
		phones = append(phones, phonesMdl[i].ToPhoneEntity())
	}

	return phones
}

func FromPhoneEntityToPhoneModel(phone entities.Phone, customerID int) PhoneModel {
	return PhoneModel{
		ID:         phone.ID,
		Number:     phone.Number,
		CustomerID: customerID,
	}
}

func FromListPhoneEntityToListPhoneModel(phones []entities.Phone, customerID int) []PhoneModel {
	phonesMdl := make([]PhoneModel, 0)

	for i := 0; i < len(phones); i++ {
		phonesMdl = append(phonesMdl, FromPhoneEntityToPhoneModel(phones[i], customerID))
	}

	return phonesMdl
}

func (pr *PhoneRepo) SaveBatch(phones []entities.Phone, customerID int) ([]entities.Phone, error) {
	phonesMdl := FromListPhoneEntityToListPhoneModel(phones, customerID)

	err := pr.DB.Model(&PhoneModel{}).CreateInBatches(phonesMdl, 10).Error
	if err != nil {
		return phones, err
	}

	phones = ToListPhoneEntity(phonesMdl)

	return phones, nil
}
