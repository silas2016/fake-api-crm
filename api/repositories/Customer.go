package repositories

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/silas2016/fake-core-crm/core/entities"
	"gorm.io/gorm"
)

type CustomerRepo struct {
	DB *gorm.DB
}

type CustomerModel struct {
	ID     int    `gorm:"column:id"`
	Name   string `gorm:"column:name"`
	Cpf    string `gorm:"column:cpf"`
	Gender string `gorm:"column:gender"`
}

func (cm *CustomerModel) TableName() string {
	return "customers"
}

func FromCustomerEntityToCustomerModel(customer entities.Customer) CustomerModel {
	return CustomerModel{
		Name:   customer.Name,
		Cpf:    customer.Cpf,
		Gender: customer.Gender,
	}
}

func (cr *CustomerRepo) Save(customer entities.Customer) (entities.Customer, error) {
	customerMdl := FromCustomerEntityToCustomerModel(customer)

	err := cr.DB.Model(&CustomerModel{}).Create(&customerMdl).Error
	if err != nil {
		return customer, err
	}

	customer.ID = customerMdl.ID

	logrus.Info("Customer saved")

	return customer, nil
}
