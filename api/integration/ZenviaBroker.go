package integration

import (
	"github.com/sirupsen/logrus"
)

type ZenviaBroker struct{}

func (zb *ZenviaBroker) SendMessage(message string, phone string) error {
	logrus.Infof("Sending message: [%s] to phone: [%s]", message, phone)

	return nil
}
