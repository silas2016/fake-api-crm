package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/silas2016/fake-api-crm/api/integration"
	"gitlab.com/silas2016/fake-api-crm/api/repositories"
	"gitlab.com/silas2016/fake-core-crm/core/dto"
	"gitlab.com/silas2016/fake-core-crm/core/services"
	"gorm.io/gorm"
)

type CustomerController struct {
	CustomerServ services.CustomerService
}

func MakeCustomerController(db *gorm.DB) CustomerController {
	customerRepo := repositories.CustomerRepo{DB: db}
	emailRepo := repositories.EmailRepo{DB: db}
	phoneRepo := repositories.PhoneRepo{DB: db}
	zenviaBroker := integration.ZenviaBroker{}

	return CustomerController{
		CustomerServ: services.MakeCustomerService(&customerRepo, &phoneRepo, &emailRepo, &zenviaBroker),
	}
}

type ErrorMessage struct {
	Error string `json:"error"`
}

func (cc *CustomerController) Create(c echo.Context) error {
	var customerDto dto.CustomerDto

	if err := c.Bind(&customerDto); err != nil {
		logrus.Error(err)
		return c.JSON(http.StatusInternalServerError, ErrorMessage{Error: "Ocorreu erro ao processar a requisição"})
	}

	customerDto, err := cc.CustomerServ.CreateCustomer(customerDto)
	if err != nil {
		logrus.Error(err)
		return c.JSON(http.StatusInternalServerError, ErrorMessage{Error: err.Error()})
	}

	return c.JSON(http.StatusCreated, customerDto)
}
